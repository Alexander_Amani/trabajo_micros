/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TICKET_BUTTON_Pin GPIO_PIN_0
#define TICKET_BUTTON_GPIO_Port GPIOA
#define TICKET_BUTTON_EXTI_IRQn EXTI0_IRQn
#define SERVO_TIMER_WATER_Pin GPIO_PIN_3
#define SERVO_TIMER_WATER_GPIO_Port GPIOA
#define SERVO_TIMER_Pin GPIO_PIN_6
#define SERVO_TIMER_GPIO_Port GPIOA
#define HCSR04_ECHO_Pin GPIO_PIN_9
#define HCSR04_ECHO_GPIO_Port GPIOE
#define LED_GREEN_Pin GPIO_PIN_9
#define LED_GREEN_GPIO_Port GPIOD
#define LED_RED_Pin GPIO_PIN_10
#define LED_RED_GPIO_Port GPIOD
#define LED_YELLOW_Pin GPIO_PIN_11
#define LED_YELLOW_GPIO_Port GPIOD
#define FC_37_DATA_Pin GPIO_PIN_12
#define FC_37_DATA_GPIO_Port GPIOD
#define FC_37_DATA_EXTI_IRQn EXTI15_10_IRQn
#define HCSR04_TRIGGER_Pin GPIO_PIN_13
#define HCSR04_TRIGGER_GPIO_Port GPIOD
#define LED_BLUE_Pin GPIO_PIN_15
#define LED_BLUE_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
