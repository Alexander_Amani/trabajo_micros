/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "hcsr04_sensor.h"// LIBRERIA DEL ULTRASONIDOS
#include "i2c-lcd.h"// LIBRERIA DEL LCR I2C

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum {ROJO, AMARILLO, VERDE}luces; //enum luces semaforo
typedef enum {S1, S2, S3, S4, S5, S6}estado; //de momento se me ocurren  6 pero podemos meter más
typedef enum {OFF, ON}pulsador; //enum para banderilla pulsador
typedef enum {NO_LLUEVE, LLUEVE}lluvia; //enum para lectura sensor lluvia
typedef enum {ESPERA, FIN}contador; //enum para banderilla de contador de tiempo
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
//defines de cotas:
#define COTA_SUP 10.0
#define COTA_INF 5.0
//defines de barrera {BAJADA O SUBIDA}:
#define BARRERA_BAJADA 110
#define BARRERA_SUBIDA 180
//defines cubierta{RECOGIDA O EXTENDIDA}:
#define CUBIERTA_RECOG 80
#define CUBIERTA_EXTEN 170
//HOJA CARACTERISTICAS SENSOR TEMP.
#define Avg_Slope .0025
#define V25  0.76
#define VSENSE 3.3/256
#define ADC_FC -0.06



/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim9;
TIM_HandleTypeDef htim12;

/* USER CODE BEGIN PV */
float dist_cm = 0;//DISTANCIA ULTRASONIDOS
//como no hace falta interrucion seria "banderilla" de lluvia:
lluvia sensor_lluvia = NO_LLUEVE;
//banderilla pulsador:
pulsador p = OFF; //comienza como no pulsado se pone a ON en el callback
				//aun estoy pensado cuando ponerlo a OFF despues de activarlo
// variable para almacenar estado:
estado s_actual=S1; //luces rojas, barrera bajada etc etc...
//banderilla de timer para barrera
contador cont_flag=ESPERA;

//bandera de temperatura
volatile int temp_flag=0;


uint8_t ADC_VAL = 0;//variable de precision de 8 bits, que toma el valor del ADC de la placa (temperatura)
float Temp = 0;// float donde se alojaran todos los valores cambiantes en la temperatura.

hcsr04_data_t HCSR04_sensor; //OBTENCION DE DATOS
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM9_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM12_Init(void);
/* USER CODE BEGIN PFP */

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance==TIM9){

		cont_flag=FIN;
	}

	else{

		HCSR04_TIM_PEC(htim);// CALLBACK PARA EVALUAR EL TIEMPO DE VUELO
	}


}

//funcion que pone las luces
void ControlLuces(luces semaforo){
	switch (semaforo){
		case ROJO:
			HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, 0);
			HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, 1);
        	HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, 0);
        	HAL_Delay(10);
			return;
		case AMARILLO:
			HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, 0);
            HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, 0);
            HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, 1);
            HAL_Delay(10);
			return;
		case VERDE:
			HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, 0);
        	HAL_GPIO_WritePin(LED_YELLOW_GPIO_Port, LED_YELLOW_Pin, 0);
			HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, 1);
			HAL_Delay(10);
			return;
		default:
			return;
	}
}

//funcion que determina el estado
//mejor desactivar banderillas aquí dentro creo: pulsador y contador ya que lluvia no es banderilla como tal.
//MEJOR PASAR POR REFERENCIA LAS BANDERILLAS PARA PODER MODIFICARLAS DENTRO
estado SiguenteEstado(float distancia, pulsador *p, lluvia sen_lluv , contador *cont_flag, estado s_actual){
	//se calcula el siguiente estado en funcion de las entradas y del estado actual
	//de momento pongo lo que se me ha ocurrido pero se pueden poner mas estado

	pulsador temp =*p;
	*p=OFF;

	switch(s_actual){
	case S1:
		if(distancia<COTA_SUP){ //si cerca
			return S3;
		}
		else if(distancia>=COTA_SUP){ //si sigo lejos
			if(sen_lluv==NO_LLUEVE) //si no llueve
				return S1; //me quedo donde estoy
			else if(sen_lluv==LLUEVE) //si llueve
				return S2; // me voy al estado "igual" pero con lluvia
		}

	case S2:
		if(distancia<COTA_SUP){ //si cerca
			return S4;
		}
		else if(distancia>=COTA_SUP){ //si sigo lejos
			if(sen_lluv==NO_LLUEVE) //si no llueve
				return S1; //"mismo estado" pero sin lluvia
			else if(sen_lluv==LLUEVE) //si llueve
				return S2; //me quedo
		}

	case S3:
		if(temp==ON){ //si he pulsado el boton (banderilla)
			//INICIAR TEMPORIZADOR PARA BAJADA DE BARRERA
			htim12.Instance->CCR1 = 255;//sonido buzzer
			HAL_Delay(100);
			htim12.Instance->CCR1 = 0;//fin sonido buzzer (max. valor 255)
			HAL_TIM_Base_Start_IT(&htim9);
			return S5;
		}
		else if(temp==OFF){ //banderilla bajada
			if(distancia>=COTA_SUP)//si lejos
				return S1;
			else if(distancia<COTA_SUP){ //si cerca
				if(sen_lluv==NO_LLUEVE)
					return S3; //me quedo
				else if(sen_lluv==LLUEVE)
					return S4; //me voy al estado con lluvia
			}
		}
	case S4:
		if(temp==ON){ //si he pulsado el boton (banderilla)
			//INICIAR TEMPORIZADOR PARA BAJADA DE BARRERA
			htim12.Instance->CCR1 = 255;//sonido buzzer
			HAL_Delay(100);
			htim12.Instance->CCR1 = 0;//fin sonido buzzer (max. valor 255)
			HAL_TIM_Base_Start_IT(&htim9);
			return S6;
	}
		else if(temp==OFF){ //banderilla bajada
			if(distancia>=COTA_SUP){//si lejos
				return S2;
			}
			else if(distancia<COTA_SUP){ //si cerca
				if(sen_lluv==NO_LLUEVE)
					return S3; //me voy a estado que no llueve
				else if(sen_lluv==LLUEVE)
					return S4; //me quedo
			}
		}
	case S5:
		if(*cont_flag==FIN){
			*cont_flag=ESPERA; //bajamos la banderilla
			HAL_TIM_Base_Stop_IT(&htim9);
			if(distancia>=COTA_SUP)
				return S1;
			else if(distancia<COTA_SUP)
				return S3;
		}
		else if(*cont_flag==ESPERA){ //si temporizador no ha finalizado
			if(sen_lluv==NO_LLUEVE)
				return S5;
			else if(sen_lluv==LLUEVE)
				return S6;
		}
	case S6:
	if(*cont_flag==FIN){
			*cont_flag=ESPERA; //bajamos la banderilla
			HAL_TIM_Base_Stop_IT(&htim9);
			if(distancia>=COTA_SUP)
				return S2;
			else if(distancia<COTA_SUP)
				return S4;
		}
		else if(*cont_flag==ESPERA){ //si temporizador no ha finalizado
			if(sen_lluv==NO_LLUEVE)
				return S5;
			else if(sen_lluv==LLUEVE)
				return S6;
		}
	}

}

//funcion que activa las salidas en funcion del estado:
//aqui iria lo de las luces, el LCD, la barrera, lo de la lluvia etc.
void ControlSalidas(estado s){
	switch(s){
	case S1: //lejos y no llueve

		ControlLuces(ROJO); //semaforo en rojo
		htim3.Instance->CCR1 = BARRERA_BAJADA; //la barrera abajo
		htim5.Instance->CCR4 = CUBIERTA_RECOG; //cubierta recogida no llueve
		lcd_put_cur(0, 0);
				lcd_send_string("Pulse el boton  ");
				lcd_put_cur(1,0);
				lcd_send_string("para pasar   ");
		return;
	case S2: // lejos y llueve

		ControlLuces(ROJO); //semaforo en rojo
		htim3.Instance->CCR1 = BARRERA_BAJADA; //la barrera abajo
		htim5.Instance->CCR4 = CUBIERTA_EXTEN; //llueve
		lcd_put_cur(0, 0);
				lcd_send_string("Pulse el boton  ");
				lcd_put_cur(1,0);
				lcd_send_string("para pasar   ");
		return;
	case S3: //estamos cerca pero sin pulsar

		ControlLuces(AMARILLO); //semaforo en ambar
		htim3.Instance->CCR1 = BARRERA_BAJADA; //la barrera abajo
		htim5.Instance->CCR4 = CUBIERTA_RECOG; //cubierta recogida no llueve


		lcd_put_cur(0, 0);
				lcd_send_string("Pulse el boton  ");
				lcd_put_cur(1,0);
				lcd_send_string("para pasar   ");
		return;
	case S4: //estamos cerca pero sin pulsar y llueve

		ControlLuces(AMARILLO); //semaforo en ambar
		htim3.Instance->CCR1 = BARRERA_BAJADA; //la barrera abajo
		htim5.Instance->CCR4 = CUBIERTA_EXTEN; //cubierta recogida no llueve


		lcd_put_cur(0, 0);
				lcd_send_string("Pulse el boton  ");
				lcd_put_cur(1,0);
				lcd_send_string("para pasar   ");
		return;
	case S5: //hemos pulsado y no llueve

		ControlLuces(VERDE); //semaforo en verde
		htim3.Instance->CCR1 = BARRERA_SUBIDA; //la barrera abajo
		htim5.Instance->CCR4 = CUBIERTA_RECOG; //cubierta recogida no llueve

			lcd_put_cur(0, 0);
			lcd_send_string("Recoja su ticket");
			lcd_put_cur(1,0);
			lcd_send_string("           ");

		return;
	case S6: //pulsado y llueve

		ControlLuces(VERDE); //semaforo en verde
		htim3.Instance->CCR1 = BARRERA_SUBIDA; //la barrera abajo
		htim5.Instance->CCR4 = CUBIERTA_EXTEN; //cubierta recogida no llueve
			lcd_put_cur(0, 0);
			lcd_send_string("Recoja su ticket");
			lcd_put_cur(1,0);
			lcd_send_string("           ");

		return;
	default:
		return;
	}
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){



	//HE INTENTADO CON ESTA LOGICA LA INTERRUPCION  Y NO PARECE FUNCIONAR...



if(GPIO_Pin==TICKET_BUTTON_Pin){


	p=ON;


//else{

	//__NOP();
//}

}
}




/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_I2C1_Init();
  MX_TIM5_Init();
  MX_TIM9_Init();
  MX_ADC1_Init();
  MX_TIM12_Init();
  /* USER CODE BEGIN 2 */
  HCSR04_Init();
  HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_4);
  HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_1);
  lcd_init();


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
                   {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  /*HAL_ADC_Start(&hadc1);
	  if(HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY)==HAL_OK){
	  		ADC_val=HAL_ADC_GetValue(&hadc1);
	  		Temp =  ((VSENSE*ADC_val - V25) / Avg_Slope) + 25;
	  		if(Temp>200){

	  			HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, 1);
	  		}
	  		else{

	  			HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, 0);
	  		}
	  	}
	  HAL_ADC_Stop(&hadc1);*/
	  HAL_ADC_Start_DMA(&hadc1, &ADC_VAL, 1);
	  Temp =  (((VSENSE*ADC_VAL - V25) / Avg_Slope) + 25)*ADC_FC;
	  	if(Temp<=5){

	  		  			HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, 1);
	  		  		}
	  		  		else{

	  		  			HAL_GPIO_WritePin(LED_BLUE_GPIO_Port, LED_BLUE_Pin, 0);
	  		  		}
	  	HAL_ADC_Stop(&hadc1);


	  	  	//lo del LCD
  	  		//leer entradas excepto lo que va por interrucion y temporizadores
  	  		HCSR04_GetInfo(&HCSR04_sensor);//OBTENCION DE INFO DEL SENSOR DE ULTRASONIDOS
  	  		sensor_lluvia=HAL_GPIO_ReadPin(FC_37_DATA_GPIO_Port, FC_37_DATA_Pin);//creo que no hace falta casting
  	  		//***************
  	  		//calculamos el proximo estado:
  	  		s_actual=SiguenteEstado(HCSR04_sensor.distance_cm, &p, sensor_lluvia, &cont_flag, s_actual);
  	  		//calculamos las salidas:
  	  		ControlSalidas(s_actual);

   }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 167;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 23323;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_GATED;
  sSlaveConfig.InputTrigger = TIM_TS_TI1FP1;
  sSlaveConfig.TriggerPolarity = TIM_TRIGGERPOLARITY_RISING;
  sSlaveConfig.TriggerFilter = 15;
  if (HAL_TIM_SlaveConfigSynchro(&htim1, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 83;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 9;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 840-1;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000-1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 840-1;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 1000-1;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */
  HAL_TIM_MspPostInit(&htim5);

}

/**
  * @brief TIM9 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM9_Init(void)
{

  /* USER CODE BEGIN TIM9_Init 0 */

  /* USER CODE END TIM9_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};

  /* USER CODE BEGIN TIM9_Init 1 */

  /* USER CODE END TIM9_Init 1 */
  htim9.Instance = TIM9;
  htim9.Init.Prescaler = 16799;
  htim9.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim9.Init.Period = 29999;
  htim9.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim9.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim9) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim9, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM9_Init 2 */

  /* USER CODE END TIM9_Init 2 */

}

/**
  * @brief TIM12 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM12_Init(void)
{

  /* USER CODE BEGIN TIM12_Init 0 */

  /* USER CODE END TIM12_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM12_Init 1 */

  /* USER CODE END TIM12_Init 1 */
  htim12.Instance = TIM12;
  htim12.Init.Prescaler = 1291-1;
  htim12.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim12.Init.Period = 255-1;
  htim12.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim12.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim12) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim12, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim12) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim12, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM12_Init 2 */

  /* USER CODE END TIM12_Init 2 */
  HAL_TIM_MspPostInit(&htim12);

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LED_GREEN_Pin|LED_RED_Pin|LED_YELLOW_Pin|HCSR04_TRIGGER_Pin
                          |LED_BLUE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : TICKET_BUTTON_Pin */
  GPIO_InitStruct.Pin = TICKET_BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(TICKET_BUTTON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_GREEN_Pin LED_RED_Pin LED_YELLOW_Pin HCSR04_TRIGGER_Pin
                           LED_BLUE_Pin */
  GPIO_InitStruct.Pin = LED_GREEN_Pin|LED_RED_Pin|LED_YELLOW_Pin|HCSR04_TRIGGER_Pin
                          |LED_BLUE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : FC_37_DATA_Pin */
  GPIO_InitStruct.Pin = FC_37_DATA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(FC_37_DATA_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
