###### Copyright ©.2020 Peaje_Toll Yago Avila/Alexander Amani/Jorge Meneu, Inc 

bitbucket.org/Alexander_Amani/trabajo_micros

##	**README**
***


###	INTRODUCTION:
***

#####Se presenta así el siguiente proyecto: sistema de peaje.
Haciendo uso de la herramienta de desarrollo Cube MX IDE, en combinación con los contenidos analizados en
la parte teórica, se creará un prototipo funcional de un sistema de peaje básico. La elección de este proyecto
permite agregar con sencillez una importante variedad de componentes hardware.
En el ámbito del software, se hará uso de nociones básicas como Interrupciones, Temporizadores, DMA y de
protocolos de comunicación serial, entre otros. Para mayor robustez en el desarrollo de versiones, se hará uso
de un repositorio alojado en Bitbucket.
Finalmente, se combinarán ambos desarrollos, en un prototipo funcional, diseñado mediante herramientas CAD
e impreso en 3D.




###	CONFIGURATION:
***

### 	_PINOUT:_
*	######	GPIO_OUTPUTS:
    +	PD9: LED_GREEN.
	+	PD10: LED_RED.
	+	PD11: LED_YELLOW.
	+	PD13: HC_SR04_TRIGGER.
	+	PD11: LED_BLUE.
	
*	######	_GPIO_EXTI:_
	+	PA0: TICKET_BUTTON.
    +	PD12: FC-37_DATA.
		
*	######	_I2C:_
	+	PB6: I2C1_SCL.
	+	PB7: I2C1_SDA.
			
*	######	_TIMERS:_
    	+	PA3: SERVO_TIMER_WATER.
	+	PA6: SERVO_TIMER.
	+	PE9: HC_SR04_ECHO.
	+	PB14: BUZZER_TIMER.
	+	PH1-OSC_IN: RCC_OSC_IN.
	+	PH1-OSC_OUT: RCC_OSC_OUT.
	
*	######	_ADC_
	+	EMBEDDED TO THE PCB.
	


	

###	_CLOCK CONFIGURATION:_
   *	######	_TO_DO:_
		+	FREQ: 168MHz.
		+	APB1: 42/84 MHz.
		+	APB2: 84/168 MHz.



###	ABOUT:
***
###### Realizado por Alexander Amani Pariamachi, Yago Avila More, Jorge Meneu Moreno.


###### Copyright ©.2020 Peaje_Toll Yago Avila/Alexander Amani/Jorge Meneu, Inc 
bitbucket.org/Alexander_Amani/trabajo_micros