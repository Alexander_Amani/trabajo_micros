###### Copyright ©.2020 Peaje_Toll Yago Avila/Alexander Amani/Jorge Meneu, Inc 

bitbucket.org/Alexander_Amani/trabajo_micros

##	**CHANGELOG**
***


###	INTRODUCTION:
***

###### _TO-DO._




###	CONFIGURATION:
***

### 	_VERSIONS:_
*	######	V4.0
    +	Versión final (A falta de pruebas).Se incluye el convertidor ADC con el sensor de temperatura embebido en la placa, el protocolo de comunicacion serie I2C y el empleo de otros componentes como el buzzer con señal de PWM. Se incluyen las librerias necesarias. 
    +	Hay problemas con el LCD. Puede ser algun tema de prioridades de interrupciones o lo más probable un problema del LCD de Meneu.

*	######	V3.0
    +	Se incluyen los sensores de lluvia y ultrasonidos, y se completan los prototipos anteriores. Se añaden los temporizadores que los controlan logicamente. Se añaden defines que plantan algunos parametros como las cotas.

*	######	V2.0
    +	Segunda versión del código. se incluyen los actuadores principales: servomotores y botón del ticket.

*	######	V1.0
    +	Primera versión del código. se ha incluido el control de los LEDs del semáforo y una función para esta.
	
	
	
###	ABOUT:
***

###### _TO-DO._


###### Copyright ©.2020 Peaje_Toll Yago Avila/Alexander Amani/Jorge Meneu, Inc 
bitbucket.org/Alexander_Amani/trabajo_micros